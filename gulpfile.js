'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    htmlmin = require('gulp-htmlmin'),
    uglify = require('gulp-uglify'),
    cssmin = require('gulp-cssmin'),
    sass = require('gulp-sass'),
    rigger = require('gulp-rigger'),
    inject = require('gulp-inject'),
    svgmin = require('gulp-svgmin'),
    svgstore = require('gulp-svgstore'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    critical = require('critical').stream,
    rimraf = require('rimraf'),
    rev = require("gulp-rev"),
    revReplace = require("gulp-rev-replace"),
    del = require('del');

var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
        svg: 'build/svg/',
        fonts: 'build/fonts/',
        uploads: 'build/uploads/',
        extras: 'build'
    },
    src: {
        html: ['src/**/*.html', '!src/templates/**/*.html', '!src/svg/*.html'],
        js: 'src/js/main.js',
        style: 'src/style/main.scss',
        img: 'src/img/**/*.*',
        svg: 'src/svg/*.svg',
        svgContainer: 'src/svg/svg.html',
        fonts: 'src/fonts/**/*.*',
        uploads: 'src/uploads/*.*',
        extras: 'src/*.*'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        style: 'src/style/**/*.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*',
        uploads: 'src/uploads/*.*',
        extras: 'src/*.*'
    },
    critical: {
        htmlSrc: 'build/**/*.html',
        base: 'build/',
        stylesSrc: 'build/css/main.css'
    },
    serverRoot: 'build',
    clean: 'build/'
};

var serverConfig = {
    server: {
        baseDir: "build"
    },
    //tunnel: true,
    host: 'localhost',
    port: 8001,
    logPrefix: "GulpRunner"
};

// for issues see https://github.com/addyosmani/critical/issues/87
gulp.task('critical', function () {
    gulp.src(path.critical.htmlSrc)
        .pipe(critical({
            base: path.critical.base,
            inline: true,
            minify: true,
            dimensions: [{
                height: 1980,
                width: 1080
            }, {
                height: 900,
                width: 600
            }, {
                height: 600,
                width: 400
            }],
            css: [path.critical.stylesSrc]
        }))
        .pipe(gulp.dest(path.critical.base));
});

gulp.task('webserver', function () {
    browserSync(serverConfig);
});

gulp.task('clean', function (cb) {
    return rimraf(path.clean, cb);
});

gulp.task('extras:build', function () {
    gulp.src(path.src.extras)
        .pipe(gulp.dest(path.build.extras));
});

gulp.task('uploads:build', function () {
    gulp.src(path.src.uploads)
        .pipe(gulp.dest(path.build.uploads))
});

gulp.task('fonts:build', function () {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(sass().on('error', sass.logError))
        .pipe(prefixer({
            browsers: ['last 4 versions'],
            cascade: false
        }))
        .pipe(cssmin())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    gulp.src(path.src.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(path.build.img));
        // .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
    var manifest = gulp.src(path.build.svg + "rev-manifest.json");

    gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(uglify())
        .pipe(revReplace({manifest: manifest}))
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('svg:build', ['clean'], function (cb) {
    del(['build'], cb);

    var svgs = gulp
        .src(path.src.svg)
        .pipe(svgstore({ inlineSvg: true }));

    function fileContents (filePath, file) {
        return file.contents.toString();
    }

    return gulp
        .src(path.src.svgContainer)
        .pipe(inject(svgs, { transform: fileContents }))
        .pipe(rev())
        .pipe(gulp.dest(path.build.svg))
        .pipe(rev.manifest())
        .pipe(gulp.dest(path.build.svg));
});

gulp.task('build', ['svg:build'], function() {
    gulp.start('html:build');
    gulp.start('js:build');
    gulp.start('fonts:build');
    gulp.start('style:build');
    gulp.start('image:build');
    gulp.start('extras:build');
    gulp.start('uploads:build');
});

gulp.task('watch', function () {
    watch([path.watch.extras], function (event, cb) {
        gulp.start('extras:build');
    });
    watch([path.watch.html], function (event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function (event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function (event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function (event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function (event, cb) {
        gulp.start('fonts:build');
    });
});


gulp.task('default', ['clean', 'svg:build', 'build', 'webserver', 'watch']);