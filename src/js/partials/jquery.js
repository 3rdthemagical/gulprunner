(function () {
    var $hamburger = $('.js-hamburger');

    $hamburger.on('click', function() {
        $(this).toggleClass('_state_active');
    });
})();